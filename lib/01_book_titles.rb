require 'byebug'
class Book
  attr_accessor :title
  # TODO: your code goes here!
  def initialize
    @title = ''
  end

  def title=(string)
    @title = fixcase(string)
  end

  def fixcase(string)
    #debugger
    exceptions = ['in', 'the', 'of', 'a', 'to', 'an', 'and']
    words = string.split(" ")
    title = words.map { |word| exceptions.include?(word) ? word : word.capitalize }.join(" ")
    title[0] = title[0].upcase
    title
  end

end
