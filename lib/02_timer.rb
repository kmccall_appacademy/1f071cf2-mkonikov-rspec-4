require 'byebug'
class Timer
  attr_accessor :seconds
  # TODO: your code goes here!
  def initialize
    @seconds = 0
  end

  def time_string
    seconds = @seconds % 60
    minutes = @seconds / 60
    hours = minutes / 60
    minutes -= hours * 60
    "#{hours}".rjust(2, '0') + ":" + "#{minutes}".rjust(2, '0') + ":" + "#{seconds}".rjust(2, '0')
  end

end
