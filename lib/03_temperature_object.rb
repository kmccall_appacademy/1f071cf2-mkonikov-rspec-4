require 'byebug'
class Temperature
  # TODO: your code goes here!
  def initialize(options)
    if options.key?(:f)
      @fahrenheit = options[:f]
    else
      @celsius = options[:c]
    end
  end

  def in_celsius
    @celsius.nil? ? ftoc : @celsius
  end

  def in_fahrenheit
    @fahrenheit.nil? ? ctof : @fahrenheit
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def ftoc
    (@fahrenheit - 32) * 0.55555555555555555
  end

  def ctof
    @celsius * 1.8 + 32
  end

end

class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end
