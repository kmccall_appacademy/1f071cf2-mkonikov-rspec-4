class Dictionary
  attr_reader :entries, :keywords
  def initialize
    @entries = {}
    @keywords = []
  end

  def add(entry)
    if entry.is_a?(Hash)
      entry = entry.to_a
      @entries[entry[0][0]] = entry[0][1]
      @keywords << entry[0][0]
    else
      @entries[entry] = nil
      @keywords << entry
    end
    @keywords.sort!
  end

  def include?(word)
    @keywords.any? {|keyword| keyword == word}
  end

  def find(word)
    @entries.select { |k,v| k.include?(word) }
  end

  def printable
    result = ''
    @keywords.each do |k|
      result << "[#{k}] \"#{@entries[k]}\"\n"
    end
    result.chomp
  end

end
